// ********************************* THIRD PARTY PACKAGES ********************************* //

const { response } = require('express');
const { validationResult } = require( 'express-validator' );
const jwt = require('jsonwebtoken');

// ********************************* MODELS ********************************* //

const Tarea = require( '../models/Tarea' );
const Proyecto = require( '../models/Proyecto' );
const { findByIdAndUpdate } = require('../models/Tarea');

// ******************************************************************************** //

exports.crearTarea = async ( req, res = response ) => {

    // VERIFY IF THERE ARE ERRORS
    const errores = validationResult( req );

    if ( !errores.isEmpty() ) {

        return res.status(400).json({
            ok: false,
            errores: errores.array()
        })

    }

    const { proyecto } = req.body;

    // EXTRACT THE PROJECT AND CHECK IF IT EXISTS
    try {
        
        const existeProyecto = await Proyecto.findById( proyecto );

        if( !existeProyecto ) {

            return res.status(404).json({
                ok: false,
                msg: 'Proyecto no encontrado'
            })

        }

        // CHECK IF THE CURRENT PROJECT BELONGS TO THE AUTHENTICATED USER
        if( existeProyecto.creador.toString() !== req.usuario.id ) {

            return res.status(401).json({
                ok: false,
                msg: 'No autorizado'
            })

        }

        // CREATE TASK
        const tarea = new Tarea( req.body );

        await tarea.save();

        res.status(200).json({
            ok: true,
            tarea
        })

        
    } catch (error) {

        console.log( error );
        
        res.status(500).json({
            ok: false,
            msg: 'Hubo un problema en el servidor.'
        })

    }


}

// GET TASK BY PROJECTS
exports.obtenerTareas = async ( req, res = response ) => {

    const { proyecto } = req.query;

    try {

        const existeProyecto = await Proyecto.findById( proyecto );

        if( !existeProyecto ) {

            return res.status(404).json({
                ok: false,
                msg: 'Proyecto no encontrado'
            })

        }

        // CHECK IF THE CURRENT PROJECT BELONGS TO THE AUTHENTICATED USER
        if( existeProyecto.creador.toString() !== req.usuario.id ) {

            return res.status(401).json({
                ok: false,
                msg: 'No autorizado'
            })

        }

        // GET TASK PROJECT
        const tareas = await Tarea.find({ proyecto }).sort({ creado: -1 });

        res.status(200).json({
            ok: true,
            tareas
        })
        
    } catch (error) {

        console.log( error );
        
        res.status(500).json({
            ok: false,
            msg: 'Hubo un problema en el servidor.'
        })
        
    }

    

}

// UPDATE TASKS
exports.actualizarTarea = async ( req, res = response ) => {

    const { proyecto, nombre, estado } = req.body;

    try {

        let tarea = await Tarea.findById( req.params.id );

        if( !tarea ){
            return res.status(404).json({
                ok: false,
                msg: 'Tarea no encontrada'
            })
        }
        
        const existeProyecto = await Proyecto.findById( proyecto );

        if( !existeProyecto ) {

            return res.status(404).json({
                ok: false,
                msg: 'Proyecto no encontrado'
            })

        }

        // CHECK IF THE CURRENT PROJECT BELONGS TO THE AUTHENTICATED USER
        if( existeProyecto.creador.toString() !== req.usuario.id ) {

            return res.status(401).json({
                ok: false,
                msg: 'No autorizado'
            })

        }

        // CREATE OBJECT WITH NEW INFORMATION
        const nuevaTarea = {}

        nuevaTarea.nombre = nombre;

        nuevaTarea.estado = estado;

        tarea = await Tarea.findByIdAndUpdate( { _id: req.params.id}, nuevaTarea, { new: true } );

        res.status(200).json({
            ok: true,
            tarea
        })

    } catch (error) {
        
        console.log( error );
        
        res.status(500).json({
            ok: false,
            msg: 'Hubo un problema en el servidor.'
        })

    }

}

// DELETE TASKS
exports.eliminarTarea = async ( req, res = response ) => {

    const { proyecto } = req.query;


    try {

        let tarea = await Tarea.findById( req.params.id );

        if( !tarea ){
            return res.status(404).json({
                ok: false,
                msg: 'Tarea no encontrada'
            })
        }
        
        const existeProyecto = await Proyecto.findById( proyecto );

        if( !existeProyecto ) {

            return res.status(404).json({
                ok: false,
                msg: 'Proyecto no encontrado'
            })

        }

        // CHECK IF THE CURRENT PROJECT BELONGS TO THE AUTHENTICATED USER
        if( existeProyecto.creador.toString() !== req.usuario.id ) {

            return res.status(401).json({
                ok: false,
                msg: 'No autorizado'
            })

        }


        await Tarea.findByIdAndDelete( { _id: req.params.id} );

        res.status(200).json({
            ok: true,
            msg: 'Tarea eliminada correctamente.'
        })

    } catch (error) {
        
        console.log( error );
        
        res.status(500).json({
            ok: false,
            msg: 'Hubo un problema en el servidor.'
        })

    }

}