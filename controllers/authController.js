// ********************************* THIRD PARTY PACKAGES ********************************* //

const bcryptjs = require('bcryptjs');
const { validationResult } = require( 'express-validator' );
const jwt = require('jsonwebtoken');

// ********************************* MODELS ********************************* //

const Usuario = require( '../models/Usuario' );

// ******************************************************************************** //

exports.autenticarUsuario = async ( req, res ) => {

    // VERIFY IF THERE ARE ERRORS
    const errores = validationResult( req );

    if ( !errores.isEmpty() ) {

        return res.status(400).json({
            ok: false,
            errores: errores.array()
        })

    }

    // GET EMAIL AND PASSWORD
    const { email, password } = req.body;

    try {

        // VERIFY THAT REGISTERED USER IS UNIQUE
        let usuario = await Usuario.findOne({ email });

        if ( !usuario ) {

            return res.status(400).json({
                ok: false,
                msg: 'Usuario no existe.'
            })

        }

        // CHECKS PASSWORD
        const passCorrecto = await bcryptjs.compare( password, usuario.password )

        if ( !passCorrecto ) {

            return res.status( 400 ).json({
                ok: false,
                msg: 'Password Incorrecto'
            })

        }
        
        // CREATE AND SIGN JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };

        // SIGN JWT
        jwt.sign( payload, process.env.SECRETA, {
            expiresIn: 3600 // SECONDS --> 1 HOUR
        }, ( error, token ) => {

            if ( error ) throw error;

            // SEND CONFIRMATION MESSAGE
            res.status(200).json({
                ok: true,
                token
            })

        })

        

    } catch (error) {
        
        console.log( error );
        res.status(500).json({
            ok: false,
            msg: 'Hubo un error.'
        })

    }

}

// GETS THE AUTHENTICATED USER
exports.usuarioAutenticado = async ( req, res) => {


    try {

        const usuario = await Usuario.findById( req.usuario.id ).select( '-password' );

        res.status( 200 ).json({
            ok: true,
            usuario
        })
        
    } catch (error) {
        
        console.error( error )

        res.status( 500 ).json({
            ok: false,
            msg: 'Hubo un error.'
        })

    }


}

