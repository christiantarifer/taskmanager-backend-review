// ********************************* THIRD PARTY PACKAGES ********************************* //

const { response } = require('express');
const { validationResult } = require( 'express-validator' );
const jwt = require('jsonwebtoken');

// ********************************* MODELS ********************************* //

const Proyecto = require( '../models/Proyecto' );

// ******************************************************************************** //

exports.crearProyecto = async (req, res = response ) => {

    // VERIFY IF THERE ARE ERRORS
    const errores = validationResult( req );

    if ( !errores.isEmpty() ) {

        return res.status(400).json({
            ok: false,
            errores: errores.array()
        })

    }

    try {

        // CREATE A PROJECT
        const proyecto = new Proyecto( req.body );

        // SAVE CREATOR VIA JWT
        proyecto.creador = req.usuario.id;

        // SAVE PROJECT
        proyecto.save();

        res.status(203).json({
            ok: true,
            msg: '¡Proyecto creado satisfactoriamente!',
            proyecto
        })
        
    } catch (error) {
        
        console.log(error);

        res.status(500).json({
            ok: false,
            msg: 'Error en el servidor, por favor contacte al administrador'
        })

    }

}

// GET ALL THE PROJECT FROM THE CURRENT USER
exports.obtenerProyectos = async ( req, res = response) => {

    try {

        const proyectos = await Proyecto.find({ creador: req.usuario.id }).sort( { creado: -1 } )

        res.status(200).json({
            ok: true,
            proyectos
        });

        
    } catch (error) {

        console.log(error);
        
        res.status(500).json({
            ok: false,
            msg: 'Error en el servidor, por favor contacte al administrador'
        })

    } 

}

// UPDATE A PROJECT
exports.actualizarProyecto = async( req, res = response ) => {

    // VERIFY IF THERE ARE ERRORS
    const errores = validationResult( req );

    if ( !errores.isEmpty() ) {

        return res.status(400).json({
            ok: false,
            errores: errores.array()
        })

    }

    // EXTRACT PROJECT INFORMATION
    const { nombre } = req.body;

    const nuevoProyecto = {  };

    if ( nombre ) {

        nuevoProyecto.nombre = nombre

    }

    try {

        // CHECK FOR THE ID
        let proyecto = await Proyecto.findById( req.params.id );

        // VERIFY THAT PROJECT EXIST
        if( !proyecto ) {

            res.status(404).json({
                ok: false,
                msg: 'Proyecto no encontrado.'
            })

        }

        // VERIFY PROJECT'S CREATOR
        if( proyecto.creador.toString() !== req.usuario.id ) {

            return res.status(401).json({
                ok: false,
                msg: 'No autorizado'
            })

        }

        // UPDATE
        proyecto = await Proyecto.findByIdAndUpdate( {_id: req.params.id  }, { $set: nuevoProyecto }, { new: true } )

        res.status(200).json({
            ok: true,
            msg: 'Proyecto actualizado',
            proyecto
        })
        
    } catch (error) {
        
        console.log(error);
        
        res.status(500).json({
            ok: false,
            msg: 'Error en el servidor, por favor contacte al administrador'
        })

    }

}

// DELETE A PROJECT
exports.eliminarProyecto = async( req, res = response ) => {

    try {

        // CHECK FOR THE ID
        let proyecto = await Proyecto.findById( req.params.id );

        // VERIFY THAT PROJECT EXIST
        if( !proyecto ) {

            res.status(404).json({
                 ok: false,
                msg: 'Proyecto no encontrado.'
            })

        }

        // VERIFY PROJECT'S CREATOR
        if( proyecto.creador.toString() !== req.usuario.id ) {

            return res.status(401).json({
                ok: false,
                msg: 'No autorizado'
            })

        }

        // DELETE PROJECT
        await Proyecto.findOneAndRemove( { _id : req.params.id } )

        res.status(200).json({
            ok: true,
            msg: 'Proyecto eliminado correctamente.',
        })
        
    } catch (error) {
        
        console.log(error);
        
        res.status(500).json({
            ok: false,
            msg: 'Error en el servidor, por favor contacte al administrador'
        })

    }

     

}