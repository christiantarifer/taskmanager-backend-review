// ********************************* THIRD PARTY PACKAGES ********************************* //

const bcryptjs = require('bcryptjs');
const { validationResult } = require( 'express-validator' );
const jwt = require('jsonwebtoken');

// ********************************* MODELS ********************************* //

const Usuario = require( '../models/Usuario' );

// ******************************************************************************** //

exports.crearUsuario = async ( req, res ) => {

    // VERIFY IF THERE ARE ERRORS
    const errores = validationResult( req );

    if ( !errores.isEmpty() ) {

        return res.status(400).json({
            ok: false,
            errores: errores.array()
        })

    }

    // GET EMAIL AND PASSWORD
    const { email, password } = req.body;

    try {

        // VERIFY THAT REGISTERED USER IS UNIQUE
        let usuario = await Usuario.findOne({ email });

        if ( usuario ) {

            return res.status(400).json({
                ok: false,
                msg: 'Usuario ya existe.'
            })

        }
        
        // CREATE USERS
        usuario = new Usuario( req.body );

        // HASHINS PASSWORD
        const salt = await bcryptjs.genSalt(10);

        usuario.password = await bcryptjs.hash( password, salt );

        // SAVE USERS
        await usuario.save();

        // CREATE AND SIGN JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };

        // SIGN JWT
        jwt.sign( payload, process.env.SECRETA, {
            expiresIn: 3600 // SECONDS --> 1 HOUR
        }, ( error, token ) => {

            if ( error ) throw error;

            // SEND CONFIRMATION MESSAGE
            res.status(200).json({
                ok: true,
                token
            })

        })

        

    } catch (error) {
        
        console.log( error );
        res.status(500).json({
            ok: false,
            msg: 'Hubo un error.'
        })

    }

} 