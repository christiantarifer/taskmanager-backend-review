// ********************************* ROUTES ********************************* //

// ********************************* THIRD PARTY PACKAGES ********************************* //

const express = require('express');

const router = express.Router();

const { check } = require('express-validator');
 

// ********************************* CUSTOM PACKAGES ********************************* //

const tareaController = require('../controllers/tareaController');
const auth = require('../middlewares/auth');

// *********************************************************************************** //

// CREATE A TASK
// api/tareas
router.post( '/',
    auth,
    [
        check('nombre', 'El nombre es obligatorio').not().isEmpty(),
        check('proyecto', 'El Proyecto es obligatorio').not().isEmpty()
    ],
    tareaController.crearTarea
)

// GET PROJECT'S TASK
router.get( '/',
    auth,
    tareaController.obtenerTareas
)

// UPDATE TASK
router.put( '/:id',
    auth,
    tareaController.actualizarTarea
)

// DELETE TASK
router.delete( '/:id',
    auth,
    tareaController.eliminarTarea
)
module.exports = router;