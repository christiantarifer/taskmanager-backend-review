// ********************************* ROUTES ********************************* //

// ********************************* THIRD PARTY PACKAGES ********************************* //

const express = require('express');

const router = express.Router();

const { check } = require('express-validator');
 

// ********************************* CUSTOM PACKAGES ********************************* //

const proyectoController = require('../controllers/proyectoController');
const auth = require('../middlewares/auth');

// *********************************************************************************** //
// CREATE A PROJECT
// api/proyecto
router.post('/',
    auth,
    [
        check('nombre', 'El nombre del proyecto es obligatorio').not().isEmpty()
    ],
    proyectoController.crearProyecto
)

// GET ALL PROJECTS
router.get('/',
    auth,
    proyectoController.obtenerProyectos
)

// UPDATE A PROJECT
router.put('/:id',
    auth,
    [
        check('nombre', 'El nombre del proyecto es obligatorio').not().isEmpty()
    ],
    proyectoController.actualizarProyecto
)

// DELETE A PROJECT
router.delete('/:id',
    auth,
    proyectoController.eliminarProyecto
)

module.exports = router;