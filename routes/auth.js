// ********************************* ROUTES ********************************* //

// ********************************* THIRD PARTY PACKAGES ********************************* //

const express = require('express');

const router = express.Router();

const { check } = require('express-validator');
 
// ********************************* MIDDLEWARE ********************************* //

const auth = require( '../middlewares/auth' );

// ********************************* CUSTOM PACKAGES ********************************* //

const authController = require('../controllers/authController');

// **************************************************************************************** //

// GET'S THE AUTHENTICATED USER
// api/auth
router.post( '/',
    authController.autenticarUsuario )

router.get('/',
    auth,
    authController.usuarioAutenticado
)

module.exports = router