// ********************************* ROUTES ********************************* //

// ********************************* THIRD PARTY PACKAGES ********************************* //

const express = require('express');

const router = express.Router();

const { check } = require('express-validator');
 

// ********************************* CUSTOM PACKAGES ********************************* //

const usuarioController = require('../controllers/usuarioController');

// **************************************************************************************** //

// CREATE A USER
// api/usuarios
router.post( '/',
    [
        check( 'nombre', 'El nombre es obligatorio.' ).not().isEmpty(),
        check( 'email', 'Agrega un email válido.' ).isEmail(),
        check( 'password', 'El password debe ser minimo de caracteres.' ).isLength( { min: 6 } )
    ],
    usuarioController.crearUsuario )

module.exports = router