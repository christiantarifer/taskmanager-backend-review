// ********************************* THIRD PARTY PACKAGES ********************************* //

const { response } = require('express');
const { validationResult } = require( 'express-validator' );
const jwt = require('jsonwebtoken');

// ********************************* MODELS ********************************* //

module.exports = function( req, res = response, next ) {

    // REAR TOKEN FROM HEADER
    const token = req.header('x-auth-token');

    // CHECK IF THERE IS A TOKEN
    if( !token ) {

        return res.status(401).json({
            ok: false,
            msg: 'No hay token, permiso no válido.'
        })

    }

    // VERIFY TOKEN
    try {

        const cifrado = jwt.verify( token, process.env.SECRETA )

        req.usuario = cifrado.usuario;

        next();

    } catch (error) {

        res.status(401).json({

            ok: false,
            msg: 'Token no válido.'

        })

    }

}