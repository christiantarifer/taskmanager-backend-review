// ***************************** THIRD PARTY PACKAGES ***************************** //

const express = require('express');
const cors = require('cors');

// ***************************** CUSTOM CONFIGURATION  ***************************** //

const conectarDB = require('./config/db');

// ******************************************************************************** //

// CREATE SERVER
const app = express();

// CONNECT TO DB
conectarDB();

// HABILITATE CORS
app.use( cors( { credentials: true, origin: true } ) );
app.options( '*', cors() );

// ACTIVATE express.json
app.use( express.json({ extended: true }) )

// APP PORT
const port = process.env.PORT || 4000;

// ROUTES
app.use( '/api/auth', require('./routes/auth') );
app.use( '/api/proyectos', require('./routes/proyectos') );
app.use( '/api/usuarios', require('./routes/usuarios') );
app.use( '/api/tareas', require('./routes/tareas') );

// START APP
app.listen( port, '0.0.0.0', () => {

    console.log( `El servidor esta funcionando en el puerto ${port}` );

});
